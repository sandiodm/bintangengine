/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Bintang Engine Main Manager
@Author Sandio Dwiko Mustika
@Since  30/07/2016
*/

#pragma once
#include "ApplicationManager.h"
#include "Config/ConfigBase.h"
#include "Core/Base.h"
#include "Core/Time.h"

BEApplicationManager::BEApplicationManager()
	: GLVerseMajor (3),
	GLVerseMinor (3)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GLVerseMajor);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GLVerseMinor);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	auto config = ConfigBase::Get();
	if (!config)
		return;

	BInt height = config->GetWindowHeight();
	BInt width = config->GetWindowWidth();
	BString gameName = config->GetGameName();

	window = glfwCreateWindow(width, height, gameName.Get(), nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		openStatus = false;
		return;
	}
	openStatus = true;
	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return;
		
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);
}

BEApplicationManager::~BEApplicationManager()
{
	glfwTerminate();
	openStatus = false;
}

BInt BEApplicationManager::GetCurrentFPS()
{
	BFloat result = 1000 / (BTime::Get()->GetDeltaTime()* 1000);
	return (BInt)result;
}

int main()
{
	///Global Settings if Any
	auto config = ConfigBase::Get();
	config->SetGameName("Hello World !");
	config->SetWindowSize(800, 640);
	///
	BEApplicationManager* instance = new BEApplicationManager();
	if (!instance->isOpen())
	{
#ifdef BINTANG_AUTHORING
		BLOG_ERROR("Failed Initiating OpenGL");
#endif
		return -1;
	}

#ifndef MASTER
	glfwSwapInterval(0);
	BLOG_CONSOLE("OpenGL version : %s\n", glGetString(GL_VERSION));
	SYSTEM_INFO siSysInfo;
	GetSystemInfo(&siSysInfo);
	BLOG_CONSOLE("Hardware information: \n");
	BLOG_CONSOLE("  Number of processors: %u\n", siSysInfo.dwNumberOfProcessors);
	BLOG_CONSOLE("  Page size: %u\n", siSysInfo.dwPageSize);
	BLOG_CONSOLE("  Processor type: %u\n", siSysInfo.dwProcessorType);
#else
	FreeConsole();
#endif
	Base* base = new Base();
	while (!glfwWindowShouldClose(instance->GetWindow()))
	{
		glfwPollEvents();
		glfwSwapBuffers(instance->GetWindow());
		BTime::Get()->Set((float)glfwGetTime());
		base->Update(BTime::Get()->GetDeltaTime());
	}

	return 0;
}