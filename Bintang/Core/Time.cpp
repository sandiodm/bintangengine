/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Anything related to Engine Time
@Author Sandio Dwiko Mustika
@Since  26/08/2016
*/

#include "Time.h"

BTime::BTime()
	: elapsedTime(0.f),
	dtTime(0.f)
{

}

BFloat BTime::GetElapsedTime()
{
	return elapsedTime;
}

BFloat BTime::GetTimeInMiliSeconds()
{
	return elapsedTime * 1000;
}

BFloat BTime::GetTimeInSeconds()
{
	return elapsedTime;
}

BFloat BTime::GetTimeInMinutes()
{
	return elapsedTime / 60;
}

BFloat BTime::GetTimeInHours()
{
	return GetTimeInMinutes() / 60;
}

BFloat BTime::GetDeltaTime()
{
	return dtTime;
}

BVoid BTime::Set(BFloat time_)
{
	if (time_ < elapsedTime)
	{
		//Resetting
		elapsedTime = 0;
	}
	BFloat lastTime = elapsedTime;
	elapsedTime = time_;
	dtTime = elapsedTime - lastTime;
}
