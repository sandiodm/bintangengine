/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Anything related to Engine Time
@Author Sandio Dwiko Mustika
@Since  26/08/2016
*/

#pragma once
#include "BEStd.h"

#ifndef BINTANG_CORE_TIME_H
#define BINTANG_CORE_TIME_H

class BTime : public Singleton<BTime>
{
public:
	BTime();
	~BTime() {};
	BFloat GetElapsedTime();												/// Returns Total Elapsed Time in Seconds
	BFloat GetTimeInMiliSeconds();											/// Returns Total Elapsed Time in MiliSeconds
	BFloat GetTimeInSeconds();												/// Returns Total Elapsed Time in Seconds
	BFloat GetTimeInMinutes();												/// Returns Total Elapsed Time in Minutes
	BFloat GetTimeInHours();												/// Returns Total Elapsed Time in Hours
	BFloat GetDeltaTime();													/// Returns Current Delta Time
	BVoid Set(BFloat time_);												/// Set Current Time. Use this if you know what you do

private:
	BFloat elapsedTime;
	BFloat dtTime;

};

#endif