/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Bintang Engine Base
@Author Sandio Dwiko Mustika
@Since  16/08/2016
*/

#pragma once
#include "BEStd.h"

#ifndef BINTANG_CORE_BASE_H
#define BINTANG_CORE_BASE_H

class Base 
{
public:
	Base() {};
	~Base() {};

	virtual BVoid Update(BFloat dtTime_) { };

};
#endif