/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Base Config of Engine
@Author Sandio Dwiko Mustika
@Since  30/07/2016
*/
#pragma once

#ifndef CONFIG_CONFIGBASE_H
#define CONFIG_CONFIGBASE_H

#include "BEStd.h"

class ConfigBase : public Singleton<ConfigBase>
{
public:

	ConfigBase() :  windowWidth(800), 
					windowHeight(600), 
					isFullScreen(false) 
	{
	}
	~ConfigBase() {}

	void SetWindowSize(BInt width, BInt height) { windowWidth = width; windowHeight = height; }
	BInt GetWindowHeight() { return windowHeight; }
	void SetWindowHeight(BInt value) { windowHeight = value; }
	BInt GetWindowWidth() { return windowWidth; }
	void SetWindowWidth(BInt value) { windowWidth = value; }
	BString GetGameName() { return gameName; }
	void SetGameName(BChar* string) { gameName = string; }
	BBool GetIsFullScreen() { return isFullScreen; }
	void SetIsFullScreen(BBool condition) { isFullScreen = condition; }

private:
	BInt windowHeight;
	BInt windowWidth;
	BString gameName;
	BBool isFullScreen;
};

#endif
