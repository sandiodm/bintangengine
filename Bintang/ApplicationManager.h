/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Bintang Engine Main Manager
@Author Sandio Dwiko Mustika
@Since  30/07/2016
*/

#ifndef BINTANG_APPLICATIONMANAGER_H
#define BINTANG_APPLICATIONMANAGER_H

#pragma once
#include "Include/BEStd.h"

class BEApplicationManager
{
public:
	BEApplicationManager();
	~BEApplicationManager();

	BBool isOpen() { return openStatus; }
	GLFWwindow* GetWindow() { return window; }
	BInt GetCurrentFPS();																	/// Get FPS Based on Current dtTime

private:
	GLFWwindow* window;
	BShort GLVerseMajor;
	BShort GLVerseMinor;
	BBool openStatus;
};

#endif