/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  Std file for BE
@Author Sandio Dwiko Mustika
@Since  30/07/2016
*/

#pragma once

#ifdef BINTANG_AUTHORING
#include <stdio.h>
#endif
#include <windows.h>

//GLEW
#define GLEW_STATIC
#include "GL/glew.h"
//GLFW
#include "GL/glfw3.h" 

#ifndef INCLUDE_BESTD
#define INCLUDE_BESTD

typedef unsigned int BEnum;
typedef unsigned int BBitfield;
typedef unsigned int BUint;
typedef int BInt;
typedef unsigned char BBool;
typedef signed char BByte;
typedef short BShort;
typedef unsigned char BUbyte;
typedef unsigned short BUshort;
typedef unsigned long BUlong;
typedef float BFloat;
typedef float BClampf;
typedef double BDouble;
typedef double BClampd;
typedef void BVoid;
typedef signed long long BInt64EXT;
typedef unsigned long long BUint64EXT;
typedef BInt64EXT  BInt_64;
typedef BUint64EXT BUint_64;
typedef char BChar;

#ifdef BINTANG_AUTHORING
#define BLOG_CONSOLE(...) printf(__VA_ARGS__)
#define BLOG_ERROR(...) BLOG_CONSOLE(__VA_ARGS__)
#else
#define BLOG_CONSOLE(...) ""
#endif



template <typename T>
class Singleton {
public:
	static T* Get()
	{
		static T *instance;
		if (!instance) instance = new T;
		return instance;
	}
};

class BString
{
public:
	friend BBool operator == (const BString & String_1, const BString & String_2)
	{
		if (String_1.str_length == String_2.str_length)
		{
			BShort counter1 = String_1.str_length;
			BInt counter2 = 0;
			BInt i = 0;
			while (i != counter1)
			{
				if (String_1.data[i] != String_2.data[i]) { counter2++; }
				i++;
			}
			if (counter2 != 0) { return false; }
			else { return true; }
		}
		else { return false; }
	}
	BString& operator = (BChar* S)
	{
		BString* data_ = new BString(S);
		*this = *data_;
		return *this;
	}
	friend BString operator +(BString &String_1, BString & String_2)
	{
		BInt size = String_1.str_length + String_2.str_length + 1;
		BChar* temp = new BChar[size];
		for (BInt i = 0; i < (String_1.str_length - 1); i++)
		{
			temp[i] = String_1.data[i];
		}

		BInt j = 0;
		for (BShort k = String_1.str_length, j = 0; k < (size - 1); k++, j++)
		{
			temp[k] = String_2.data[j];
		}
		if (temp[size - 1] != '\0') { temp[size - 1] = '\0'; }

		BString String_3;
		String_3.str_length = size;
		delete[] String_3.data;
		String_3.data = new BChar[size];
		for (BInt z = 0; z < (size - 1); z++)
		{
			String_3.data[z] = temp[z];
		}
		if (String_3.data[size - 1] != '\0') { String_3.data = '\0'; }
		return String_3;
		delete[] temp;
	}

	BString()
	{
		str_length = 0;
		data = new BChar[str_length];
	};
	BString(BChar *STR)
	{
		BInt n = 0;
		for (const BChar* p = STR; *p != '\0'; ++p, ++n);
		str_length = n;
		data = new BChar[str_length];
		data = STR;
	}
	BString(BString & STR)
	{
		str_length = STR.GetLength();
		data = STR.Get();
	}
	virtual ~BString()
	{
		//Memory Leak
		//delete[] data;
	}

	BBool isEmpty()
	{
		return GetLength() > 0 ? false : true;
	}

	BChar* Get() { return data; }
	BInt GetLength() { return str_length; }
	void Set(BChar * input)
	{
		BInt n = 0;
		for (const BChar* p = input; *p != '\0'; ++p, ++n);
		str_length = n;
		data = input;
	}
private:
	BShort str_length;
	BChar *data;
};

#endif