# README #

This README would normally document whatever steps are necessary to get your application up and running.

# BINTANG ENGINE #

## Quick summary ##
Bintang Engine is a Game Engine made by Kasan Entertainment.
Main purpose of this engine's are investing on contributor's skill.
And for each version of this engine should be used by minimum of 2 released game.

## Version 1.0 Feature ##
- Component Based Engine
- 2D Graphic
- OpenGL 3.0
- Controller Support (Keyboard and Mouse)
- Physics
- Animation Support
- File Read/Write Support
- Save/Load Support
- PC Windows Support

## Current Contributor ##
- Sandio Dwiko Mustika          Contact@sandiodwi.co


# For First Timer #

* Set Up Everything
- install Visual Studio on your PC (Preferable Community Edition 2015)
- git / Bitbucket SourceTree (if you unfamiliar with these, go [HERE](https://www.atlassian.com/git/tutorials/)
- Clone this repository (HTTPS/SSH)
- Run .sln file
- Compile, and if there is no error, you can start explore
- if there is any error, message any contact person listed at the bottom of this file


# Contribution Guidelines #

## * Writing Guide ##
### 1. For every class you made/handle, you should give this comment on top of it : ###

```
#!c++

/*
Copyright 2016 by Kasan Entertainment, Please do not copy
or monetize any part of these code, and anything built with
this code since it's prohibited by law. Those who disobey
this rule will processed by law and might get a sever
e punishment according to the law.
Hak cipta 2016 oleh Kasan Entertainment, Dimohon untuk tidak
menerbitkan, atau bahkan menguangkan bagian manapun dari
code tersebut. Mereka yang melanggar aturan ini akan di -
proses oleh hukum dan akan mendapatkan hukuman seberat -
beratnya sesuai hukum yang tertera.

@Brief  [Purpose]
@Author [Your Name]
@Since  [Date]
*/
```
Brief: purpose of class/file
Author: Your name
Since: The date you created

This will make it easier to track if anything goes wrong
### 2. Make Sure Debug Code are wrapped inside **BINTANG_AUTHORING** ###
in case you want to put Log, or feature that is not for Master version.
Code inside this macro will not be compiled for Master Version.
Example:

```
#!c++

#ifdef BINTANG_AUTHORING //All Platform
    //Only run in BE_Debug
#elif MASTER //All Platform
    //Only run in MASTER
#endif
```

### 3. If a push crashing Master Repo, we will rollback your change. ###

### CONTACT ###

* Any Questions : Contact@sandiodwi.co